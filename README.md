# Python Web Functions

Simple helper class for exposing Python functions to the web.

# Quickstart
1. download [functionwebexpose.py](https://gitlab.com/benjamin.servoz/python-web-functions/raw/master/functionwebexpose.py)
2. sample use:
```python
import functionwebexpose
import time

# defining function we want to expose to the web
def mouse_click_function():
	print("mouse click")

def mouse_down_function():
	print("mouse down")

def mouse_up_function():
	print("mouse up")

def one_parameter_function( param1 ):
	print("got 1 param: " + param1)

def two_parameter_function( param1, param2 ):
	print("got 2 params: " + param1 + ", and: " + param2)

def mouse_click_function_with_return():
	print("mouse click with return")
	return "this is what's returned"

def key_a_down():
    print("a key is down")

def key_a_up():
    print("a key is up")

# the meat
try:
    functionwebexpose.server_start()
    functionwebexpose.register( "<button>This button triggers a function</button><br/>", mouse_click_function )
    functionwebexpose.register( "<button>This button triggers 2 functions</button><br/>", mouse_down_function, mouse_up_function )
    functionwebexpose.register( "<button>This button triggers a function and gives it a parameter</button><input type=\"text\" placeholder=\"some parameter\" value=\"some parameter\"/><br>", one_parameter_function )
    functionwebexpose.register( "<button>This button triggers a function and gives it 2 parameters</button><input type=\"text\" placeholder=\"some parameter\" value=\"some parameter\"/><input type=\"text\" placeholder=\"some other parameter\" value=\"some other parameter\"/><br>", two_parameter_function )
    functionwebexpose.arbitrary_html( "<br/><br/><center>This is some arbitrary HTML</center><br/><br/>" )
    functionwebexpose.register( "<button>This button calls a function with a return value</button><br/>", mouse_click_function_with_return )
    functionwebexpose.bind_key( "a", key_a_down, key_a_up )
    while True:
        # your program can do other things here, the web component is in its own thread
	    time.sleep( 1 )
except KeyboardInterrupt: # ctrl+c
    pass

# the program is finished
functionwebexpose.server_stop()
```
3. Point your browser to http://localhost:5005 (replace localhost with IP if remote)