import http.server
import threading
import urllib.parse

exposed_functions = []

def handle_get(path, postvars={}):
    # Construct and send basic HTTP to the client
    # Preamble
    output = 'HTTP/1.0 200 OK\r\n'
    output += 'Content-Type: text/html\r\n\r\n'
    for exposed_function in exposed_functions:
        if exposed_function['function']!=None and exposed_function['arguments']!=None and "/"+exposed_function['function'].__name__==path:
            return_value = None
            arguments = []
            for argument in exposed_function['arguments']:
                arguments.append( postvars[argument] )
            return_value = exposed_function['function']( *arguments )
            if return_value!=None:
                output += str(return_value)
            # the function could have been registered more than once and we don't want to call it more than once
            break ;

    # Beginning of html
    if path=="/":
        output += '<html>\n'
        output += '<head>\n'
        output += '<title>Python Web Control</title>\n'
        output += '<script>touch=false</script>\n'
        output += '<script>function getElementsStartsWithId( id ) {var children = document.body.getElementsByTagName("*");var elements = [], child;for (var i = 0, length = children.length; i < length; i++) {child = children[i];if (child.id.substr(0, id.length) == id)elements.push(child);}return elements;}</script>'
        output += '<script>function ajax_call(path) {var elements=getElementsStartsWithId(path+"_arg_");data="";for(i=0;i<elements.length;i++){data+=elements[i].id+"="+elements[i].value+"&"};data=data.slice(0,-1);var xhttp=new XMLHttpRequest();xhttp.open("POST", path, true);xhttp.onload=function(e){if(xhttp.readyState==4){if(xhttp.status==200){document.getElementById("status").innerHTML=xhttp.responseText;} else {console.error(xhttp.statusText);}}};xhttp.onError=function(e){console.error(xhttp.statusText);};xhttp.send(data); }</script>\n'
        output += '<script>function draw_turtle_code(o,t,i,a,_,r){i=void 0!==i?i:0,a=void 0!==a&&a,_=void 0!==_&&_,r=void 0!==r&&r;var e=t.getContext("2d");0==i&&(e.clearRect(0,0,t.width,t.height),o=(o=(o=o.replace(" ","")).toLowerCase()).split("\\n")),normalizing_ratio_X=1*t.width/100,normalizing_ratio_Y=1*t.height/100;for(var n=i;n<o.length;n++){var m=o[n];if(2==(m=m.split("(")).length&&(command=m[0],params=m[1].replace(")","").split(","),"go_to"==command&&(!1===a&&(a=50),!1===_&&(_=50),to_X=!1,to_Y=!1,void 0!==params[0]&&(to_X=parseFloat(params[0])),void 0!==params[1]&&(to_Y=parseFloat(params[1])),!1!==a&&!1!==_&&!1!==to_X&&!1!==to_Y&&(to_X_orig=to_X,to_Y_orig=to_Y,a*=normalizing_ratio_X,_=t.height-_*normalizing_ratio_Y,to_X*=normalizing_ratio_X,to_Y=t.height-to_Y*normalizing_ratio_Y,!0===r&&(e.beginPath(),e.moveTo(a,_),e.lineTo(to_X,to_Y),e.stroke()),a=to_X_orig,_=to_Y_orig)),"pen_up"==command&&(r=!1),"pen_down"==command&&(r=!0)),n%100==0&&n<o.length-1)return setTimeout(function(){draw_turtle_code(o,t,n+1,a,_,r)},10),!1}}</script>'
        output += '<script>function gcode_to_turtle(t){var e=!1,o=!1,n=!1,s=!1,i=[];t=t.split("(Start cutting path id:");for(var a=1;a<t.length;a++){path=t[a],path=path.split("(End cutting path id:"),path=path[0],path=path.split("\\n");for(var r={cords:[]},d=0;d<path.length;d++){line=path[d],line=line.split(" "),X=!1,Y=!1;for(var h=0;h<line.length;h++){item=line[h];var c=item.substring(0,1),p=item.substring(1);"X"==c?X=parseFloat(p):"Y"==c&&(Y=parseFloat(p))}!1!==X&&!1!==Y&&(!1!==X&&(X<e||!1===e)&&(e=X),!1!==Y&&(Y<o||!1===o)&&(o=Y),!1!==X&&(X>n||!1===n)&&(n=X),!1!==Y&&(Y>s||!1===s)&&(s=Y),"origin"in r||(r.origin={x:X,y:Y}),r.cords.push({x:X,y:Y}))}r.destination={x:r.cords[r.cords.length-1].x,y:r.cords[r.cords.length-1].y},i.push(r)}var _=70/(n-e),l=70/(s-o),g=Math.min(_,l);for(a=0;a<i.length;a++)for(i[a].origin.x=(i[a].origin.x-e)*g+15,i[a].origin.y=(i[a].origin.y-o)*g+15,i[a].destination.x=(i[a].destination.x-e)*g+15,i[a].destination.y=(i[a].destination.y-o)*g+15,d=0;d<i[a].cords.length;d++)i[a].cords[d].x=(i[a].cords[d].x-e)*g+15,i[a].cords[d].y=(i[a].cords[d].y-o)*g+15;var x=0;for(a=1;a<i.length;a++)x+=Math.sqrt(Math.pow(i[a].origin.x-i[a-1].destination.x,2)+Math.pow(i[a-1].origin.y-i[a-1].destination.y,2));console.log("> distance_between_penstrokes before optimization"+x);var y=[];for(y.push(i.shift());0<i.length;){var v=y[y.length-1].destination.x,u=y[y.length-1].destination.y;for(closest_path_index=!1,closest_path_distance=!1,closest_path_reverse=!1,a=0;a<i.length;a++){var f=Math.sqrt(Math.pow(i[a].origin.x-v,2)+Math.pow(i[a].origin.y-u,2)),M=Math.sqrt(Math.pow(i[a].destination.x-v,2)+Math.pow(i[a].destination.y-u,2));(!1===closest_path_distance||f<closest_path_distance||M<closest_path_distance)&&(closest_path_reverse=M<f?(closest_path_distance=M,!0):(closest_path_distance=f,!1),closest_path_index=a)}if(closest_path_reverse){var m=i[closest_path_index].origin,w=i[closest_path_index].destination,b=i[closest_path_index].cords;b=b.reverse(),i[closest_path_index].origin=w,i[closest_path_index].destination=m,i[closest_path_index].cords=b}y.push(i[closest_path_index]),i.splice(closest_path_index,1)}x=0;for(a=1;a<y.length;a++)x+=Math.sqrt(Math.pow(y[a].origin.x-y[a-1].destination.x,2)+Math.pow(y[a-1].origin.y-y[a-1].destination.y,2));console.log("> distance_between_penstrokes after optimization"+x);var q="";q+="pen_up()\\n";v=!1,u=!1;var E=!1;for(a=0;a<y.length;a++){for(d=0;d<y[a].cords.length;d++)v=y[a].cords[d].x,u=y[a].cords[d].y,E&&0==d||(q+="go_to( "+y[a].cords[d].x+", "+y[a].cords[d].y+" )\\n"),0!=d||E||(q+="pen_down()\\n"),E=E&&!1;a<y.length-1&&1<=y[a+1].cords.length&&y[a+1].cords[0].x==v&&y[a+1].cords[0].y==u?E=!0:q+="pen_up()\\n"}document.getElementById("parse_go_to_code_arg_0").value=q,draw_turtle_code(document.getElementById("parse_go_to_code_arg_0").value,document.getElementById("canvas"))}</script>'
        output += '<style>body {-webkit-user-select: none;-moz-user-select: -moz-none;-ms-user-select: none;user-select: none;}</style>\n'
        output += '</head>\n'
        # Beginning of body
        output += '<body>\n'
        for exposed_function in exposed_functions:
            output += exposed_function['html'] + '\n'
        output += '<div id="status_wrapper" style="padding:10px;"><pre>Result:<div id="status"></div></pre></div>\n'
        output += '</body>\n'
        output += '</html>\n'
    return output

class MyHandler(http.server.BaseHTTPRequestHandler):
    def do_GET(s):
        output = handle_get(s.path)
        s.wfile.write(output.encode())
    def do_POST(s):
        postvars = {}
        for key,value in list(dict(urllib.parse.parse_qs(s.rfile.read(int(s.headers['Content-Length'])))).items()):
            postvars[key.decode('ascii')] = value[0].decode('ascii')
        output = handle_get(s.path, postvars)
        s.wfile.write(output.encode())

# Listen on all IP addresses, using tcp port 5005
host, port = '0.0.0.0', 5005

# Instantiate the server object and handler
server = http.server.HTTPServer((host, port), MyHandler)

server_thread = threading.Thread(target=server.serve_forever)
server_thread.daemon = True


def arbitrary_html( html ):
    exposed_functions.append( {'html':html, 'function':None, 'arguments':None} )


def bind_key( key, keydown_function, keyup_function=None ):
    keydown_html = '<script>var '+key+'down=false;window.addEventListener("keydown",function(e){if(e.key=="'+key+'" && '+key+'down===false) {'+key+'down=true;console.log("key "+e.key+" down");ajax_call("'+keydown_function.__name__+'");}})</script>' ;
    exposed_functions.append( {'html':keydown_html, 'function':keydown_function, 'arguments':[]} )
    if keyup_function==None:
        keyup_html = '<script>window.addEventListener("keyup",function(e){if(e.key=="'+key+'") {'+key+'down=false;console.log("key "+e.key+" up") ;}})</script>' ;
        exposed_functions.append( {'html':keyup_html, 'function':None, 'arguments':None} )
    else:
        keyup_html = '<script>window.addEventListener("keyup",function(e){if(e.key=="'+key+'") {'+key+'down=false;console.log("key "+e.key+" up") ;ajax_call("'+keyup_function.__name__+'");}})</script>' ;
        exposed_functions.append( {'html':keyup_html, 'function':keyup_function, 'arguments':[]} )


def register( html, mousedown_function, mouseup_function=None ):
    button_index = html.lower().find( "<button" )
    if button_index!=-1:
        if mouseup_function!=None:
            html = html[:button_index] + "<button onMouseDown=\"if(touch){return false;}ajax_call('"+mousedown_function.__name__+"');\" onTouchStart=\"touch=true;ajax_call('"+mousedown_function.__name__+"');\" onMouseUp=\"if(touch){return false;}ajax_call('"+mouseup_function.__name__+"');\" onTouchEnd=\"touch=true;ajax_call('"+mouseup_function.__name__+"');\"" + html[button_index+7:]
        else:
            html = html[:button_index] + "<button onClick=\"ajax_call('"+mousedown_function.__name__+"');\" " + html[button_index+7:]

    arguments = []
    offset = 0
    input_index = html.lower().find( "<input", offset )
    while input_index!=-1:
        argument_id = mousedown_function.__name__ + "_arg_" + str(offset) ;
        html = html[:input_index] + "<input id=\"" + argument_id + "\" " + html[input_index+6:]
        arguments.append( argument_id )
        offset = input_index + 6
        input_index = html.lower().find( "<input", offset )

    input_index = html.lower().find( "<textarea", offset )
    while input_index!=-1:
        argument_id = mousedown_function.__name__ + "_arg_" + str(offset) ;
        html = html[:input_index] + "<textarea id=\"" + argument_id + "\" " + html[input_index+6:]
        arguments.append( argument_id )
        offset = input_index + 9
        input_index = html.lower().find( "<textarea", offset )

    exposed_functions.append( {'html':html, 'function':mousedown_function, 'arguments':arguments} )
    if mouseup_function!=None:
         exposed_functions.append( {'html':"", 'function':mouseup_function, 'arguments':[]} )


def server_start():
    server_thread.start()
    print( "> web server has been started" )

def server_stop():
    server.shutdown()
    server.server_close()
    print( "> web server has been stopped" )