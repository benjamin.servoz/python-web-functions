import BaseHTTPServer
import threading
import urlparse

exposed_functions = []

def handle_get(path, postvars={}):
    # Construct and send basic HTTP to the client
    # Preamble
    output = 'HTTP/1.0 200 OK\r\n'
    output += 'Content-Type: text/html\r\n\r\n'
    for exposed_function in exposed_functions:
        if exposed_function['function']!=None and exposed_function['arguments']!=None and "/"+exposed_function['function'].__name__==path:
            return_value = None
            arguments = []
            for argument in exposed_function['arguments']:
                arguments.append( postvars[argument] )
            return_value = exposed_function['function']( *arguments )
            if return_value!=None:
                output += str(return_value)
            # the function could have been registered more than once and we don't want to call it more than once
            break ;

    # Beginning of html
    if path=="/":
        output += '<html>\n'
        output += '<head>\n'
        output += '<title>Python Web Control</title>\n'
        output += '<script>touch=false</script>\n'
        output += '<script>function getElementsStartsWithId( id ) {var children = document.body.getElementsByTagName("*");var elements = [], child;for (var i = 0, length = children.length; i < length; i++) {child = children[i];if (child.id.substr(0, id.length) == id)elements.push(child);}return elements;}</script>'
        output += '<script>function ajax_call(path) {var elements=getElementsStartsWithId(path+"_arg_");data="";for(i=0;i<elements.length;i++){data+=elements[i].id+"="+elements[i].value+"&"};data=data.slice(0,-1);var xhttp=new XMLHttpRequest();xhttp.open("POST", path, true);xhttp.onload=function(e){if(xhttp.readyState==4){if(xhttp.status==200){document.getElementById("status").innerHTML=xhttp.responseText;} else {console.error(xhttp.statusText);}}};xhttp.onError=function(e){console.error(xhttp.statusText);};xhttp.send(data); }</script>\n'
        output += '<style>body {-webkit-user-select: none;-moz-user-select: -moz-none;-ms-user-select: none;user-select: none;}</style>\n'
        output += '</head>\n'
        # Beginning of body
        output += '<body>\n'
        for exposed_function in exposed_functions:
            output += exposed_function['html'] + '\n'
        output += '<div id="status_wrapper" style="padding:10px;"><pre>Result:<div id="status"></div></pre></div>\n'
        output += '</body>\n'
        output += '</html>\n'
    return output

class MyHandler(BaseHTTPServer.BaseHTTPRequestHandler):
    def do_GET(s):
        output = handle_get(s.path)
        s.wfile.write(output)
    def do_POST(s):
        postvars = {}
        for key,value in dict(urlparse.parse_qs(s.rfile.read(int(s.headers['Content-Length'])))).items():
            postvars[key] = value[0]
        output = handle_get(s.path, postvars)
        s.wfile.write(output)

# Listen on all IP addresses, using tcp port 5005
host, port = '0.0.0.0', 5005

# Instantiate the server object and handler
server = BaseHTTPServer.HTTPServer((host, port), MyHandler)

server_thread = threading.Thread(target=server.serve_forever)
server_thread.daemon = True


def arbitrary_html( html ):
    exposed_functions.append( {'html':html, 'function':None, 'arguments':None} )


def bind_key( key, keydown_function, keyup_function=None ):
    keydown_html = '<script>var '+key+'down=false;window.addEventListener("keydown",function(e){if(e.key=="'+key+'" && '+key+'down===false) {'+key+'down=true;console.log("key "+e.key+" down");ajax_call("'+keydown_function.__name__+'");}})</script>' ;
    exposed_functions.append( {'html':keydown_html, 'function':keydown_function, 'arguments':[]} )
    if keyup_function==None:
        keyup_html = '<script>window.addEventListener("keyup",function(e){if(e.key=="'+key+'") {'+key+'down=false;console.log("key "+e.key+" up") ;}})</script>' ;
        exposed_functions.append( {'html':keyup_html, 'function':None, 'arguments':None} )
    else:
        keyup_html = '<script>window.addEventListener("keyup",function(e){if(e.key=="'+key+'") {'+key+'down=false;console.log("key "+e.key+" up") ;ajax_call("'+keyup_function.__name__+'");}})</script>' ;
        exposed_functions.append( {'html':keyup_html, 'function':keyup_function, 'arguments':[]} )


def register( html, mousedown_function, mouseup_function=None ):
    button_index = html.lower().find( "<button" )
    if button_index!=-1:
        if mouseup_function!=None:
            html = html[:button_index] + "<button onMouseDown=\"if(touch){return false;}ajax_call('"+mousedown_function.__name__+"');\" onTouchStart=\"touch=true;ajax_call('"+mousedown_function.__name__+"');\" onMouseUp=\"if(touch){return false;}ajax_call('"+mouseup_function.__name__+"');\" onTouchEnd=\"touch=true;ajax_call('"+mouseup_function.__name__+"');\"" + html[button_index+7:]
        else:
            html = html[:button_index] + "<button onClick=\"ajax_call('"+mousedown_function.__name__+"');\" " + html[button_index+7:]

    arguments = []
    offset = 0
    input_index = html.lower().find( "<input", offset )
    while input_index!=-1:
        argument_id = mousedown_function.__name__ + "_arg_" + str(offset) ;
        html = html[:input_index] + "<input id=\"" + argument_id + "\" " + html[input_index+6:]
        arguments.append( argument_id )
        offset = input_index + 6
        input_index = html.lower().find( "<input", offset )

    input_index = html.lower().find( "<textarea", offset )
    while input_index!=-1:
        argument_id = mousedown_function.__name__ + "_arg_" + str(offset) ;
        html = html[:input_index] + "<textarea id=\"" + argument_id + "\" " + html[input_index+6:]
        arguments.append( argument_id )
        offset = input_index + 9
        input_index = html.lower().find( "<textarea", offset )

    exposed_functions.append( {'html':html, 'function':mousedown_function, 'arguments':arguments} )
    if mouseup_function!=None:
         exposed_functions.append( {'html':"", 'function':mouseup_function, 'arguments':[]} )


def server_start():
    server_thread.start()
    print "> web server has been started"

def server_stop():
    server.shutdown()
    server.server_close()
    print "> web server has been stopped"
